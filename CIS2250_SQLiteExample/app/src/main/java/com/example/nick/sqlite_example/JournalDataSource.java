package com.example.nick.sqlite_example;

/**
 * Created by Nick on 1/17/2017.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class JournalDataSource {

    // Database fields
    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;
    private String[] allColumns = { MySQLiteHelper.COLUMN_ID,
            MySQLiteHelper.COLUMN_TITLE, MySQLiteHelper.COLUMN_DATE, MySQLiteHelper.COLUMN_CONTENT};

    public JournalDataSource(Context context) {
        dbHelper = new MySQLiteHelper(context);
    }



    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Journal createJournal(String date, String title, String content) {
        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.COLUMN_TITLE, title);
        values.put(MySQLiteHelper.COLUMN_DATE, date);
        values.put(MySQLiteHelper.COLUMN_CONTENT, content);
        long insertId = database.insert(MySQLiteHelper.TABLE_JOURNAL_ENTRIES, null,
                values);
        Cursor cursor = database.query(MySQLiteHelper.TABLE_JOURNAL_ENTRIES,
                allColumns, MySQLiteHelper.COLUMN_ID + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        Journal newJournal = cursorToJournal(cursor);
        cursor.close();
        return newJournal;
    }

    public void deleteJournal(Journal journal) {
        long id = journal.getId();
        System.out.println("Comment deleted with id: " + id);
        database.delete(MySQLiteHelper.TABLE_JOURNAL_ENTRIES, MySQLiteHelper.COLUMN_ID
                + " = " + id, null);
    }

    public List<Journal> getAllJournals() {
        List<Journal> entries = new ArrayList<Journal>();

        Cursor cursor = database.query(MySQLiteHelper.TABLE_JOURNAL_ENTRIES,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Journal journal = cursorToJournal(cursor);
            entries.add(journal);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return entries;
    }

    private Journal cursorToJournal(Cursor cursor) {
        Journal journal = new Journal();
        journal.setId(cursor.getLong(0));
        journal.setTitle(cursor.getString(1));
        journal.setDate(cursor.getString(2));
        journal.setContent(cursor.getString(3));
        return journal;
    }
}